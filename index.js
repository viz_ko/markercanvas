(function () {
    const canvas = document.getElementById("marker-canvas");
    const markerCanvas = new MarkerCanvas(canvas, "img/test1.jpg", "img/ic_circle.png");

    const markerData = [{
        x: 0.1,
        y: 0.2,
        text: 1
    }, {
        x: 0.6,
        y: 0.1,
        text: 2
    }, {
        x: 0.5,
        y: 0.3,
        text: 3
    }, {
        x: 0.72,
        y: 0.83,
        text: 4
    }];

    document.getElementById("zoom-in").addEventListener("click", () => {
        markerCanvas.zoomIn();
    });

    document.getElementById("zoom-out").addEventListener("click", () => {
        markerCanvas.zoomOut();
    });

    document.getElementById("add-marker-btn").addEventListener("click", () => {
        markerCanvas.addMarkerStart();
    });

    document.getElementById("remove-marker-btn").addEventListener("click", () => {
        if (markerCanvas.selectedMarker) {
            markerCanvas.removeMarker(markerCanvas.selectedMarker);
        }
    });

    document.getElementById("move-marker-btn").addEventListener("click", () => {
        markerCanvas.moveMarkerStart();
    });

    document.getElementById("modify-marker-btn").addEventListener("click", () => {
        if (markerCanvas.selectedMarker) {
            // modify example
            console.log(markerCanvas.selectedMarker.data);
            markerCanvas.modifyMarker(markerCanvas.selectedMarker, {
                text: 7,
                x: 0.5,
                y: 0.5
            });
        }
    });

    document.getElementById("cancel-btn").addEventListener("click", () => {
        // should roll back to initial
        markerCanvas.clearAllMarkers();
        // do custom job here, or just load initial markers
    });

    document.getElementById("save-btn").addEventListener("click", () => {
        console.log(markerCanvas.extractMarkerData());
    });

    // add initial example
    markerCanvas.onload = () => {
        markerData.forEach(data => {
            markerCanvas.addMarker(data);
        });
        markerCanvas.markerLastCount = 4;
    }
})()