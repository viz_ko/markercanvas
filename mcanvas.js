function MarkerCanvas(canvas, mainImagePath, markerImagePath, markerImageScale) {
    this.canvas = canvas;
    this.mainImagePath = mainImagePath;
    this.markerImagePath = markerImagePath;
    this.markerImageScale = markerImageScale || 0.27;
    this.init();
}

// helper method for async image loading
MarkerCanvas.loadImage = uri => {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.onload = () => resolve(img);
        img.onerror = () => reject(new Error("Failed to load ${uri}!"));
        img.src = uri;
    });
};

MarkerCanvas.prototype.init = function () {
    Promise.all([
        MarkerCanvas.loadImage(this.mainImagePath),
        MarkerCanvas.loadImage(this.markerImagePath)
    ]).then(images => {
        // name images
        this.mainImage = images[0];
        this.markerImage = images[1];

        // setup canvas
        this.canvas.width = window.innerWidth;
        this.canvas.height = this.canvas.width / this.mainImage.width * this.mainImage.height;

        // init stage
        this.stage = new createjs.Stage(this.canvas);
        this.mainBmp = new createjs.Bitmap(this.mainImage);
        this.mainBmp.fitScale = window.innerWidth / this.mainImage.width;
        this.mainBmp.scale = this.mainBmp.fitScale;
        this.calcMainBmpSize();
        this.stage.addChild(this.mainBmp);

        // init marker related
        this.markers = []
        this.addMarkerFlag = false
        this.moveMarkerFlag = false
        this.markerLastCount = 0
        this.selectedMarker = null

        // add event listeners
        // enable touch interactions if supported
        createjs.Touch.enable(this.stage);
        this.stage.on("mousedown", this.onMouseDown.bind(this));
        this.stage.on("pressmove", this.onPressMove.bind(this));

        // add ticker
        createjs.Ticker.addEventListener("tick", this.handleTick.bind(this));

        // apply initial update
        this.hasUpdate = true;

        // call onload
        if (this.onload) {
            this.onload();
        }
    });
}

MarkerCanvas.prototype.handleTick = function () {
    if (this.hasUpdate) {
        this.hasUpdate = false;
        this.stage.update();
    }
}

MarkerCanvas.prototype.zoomIn = function () {
    if (this.mainBmp.scale < 2 * this.mainBmp.fitScale) {
        this.mainBmp.scale *= 1.1;
    } else {
        this.mainBmp.scale = 2 * this.mainBmp.fitScale
    }
    this.calcMainBmpSize();
    this.limitOffsetOnBorder();
    this.calcMarkerLocation();
    this.hasUpdate = true;
}

MarkerCanvas.prototype.zoomOut = function () {
    if (this.mainBmp.scale > this.mainBmp.fitScale) {
        this.mainBmp.scale /= 1.1;
    } else {
        this.mainBmp.scale = this.mainBmp.fitScale
    }
    this.calcMainBmpSize();
    this.limitOffsetOnBorder();
    this.calcMarkerLocation();
    this.hasUpdate = true;
}

MarkerCanvas.prototype.calcMainBmpSize = function () {
    this.mainBmp.width = this.mainBmp.scale * this.mainImage.width;
    this.mainBmp.height = this.mainBmp.scale * this.mainImage.height;
    this.mainBmp.xLimit = -(this.mainBmp.width - this.canvas.width)
    this.mainBmp.yLimit = -(this.mainBmp.height - this.canvas.height)
}

MarkerCanvas.prototype.limitOffsetOnBorder = function () {
    // set border limit
    if (this.mainBmp.x > 0)
        this.mainBmp.x = 0;
    if (this.mainBmp.x < this.mainBmp.xLimit)
        this.mainBmp.x = this.mainBmp.xLimit;
    if (this.mainBmp.y > 0)
        this.mainBmp.y = 0;
    if (this.mainBmp.y < this.mainBmp.yLimit)
        this.mainBmp.y = this.mainBmp.yLimit;
}

MarkerCanvas.prototype.onMouseDown = function (event) {
    if (this.addMarkerFlag) {
        this.addMarkerFlag = false;
        this.markerLastCount += 1;
        const relLoc = this.calcRelativeLocationInMain(event.stageX, event.stageY);
        this.addMarker({
            text: this.markerLastCount,
            x: relLoc.x,
            y: relLoc.y
        })
    } else {
        this.prevMoveX = event.stageX
        this.prevMoveY = event.stageY
    }
    // on touching canvas area other than markers
    // clear selected marker
    this.selectedMarker = null
}

MarkerCanvas.prototype.onPressMove = function (event) {
    // only if the stage is zoomed in
    if (this.mainBmp.scale > this.mainBmp.fitScale) {
        this.mainBmp.x += (event.stageX - this.prevMoveX);
        this.mainBmp.y += (event.stageY - this.prevMoveY);
        this.prevMoveX = event.stageX;
        this.prevMoveY = event.stageY;
        this.limitOffsetOnBorder();
        this.calcMarkerLocation();
        this.hasUpdate = true;
    }
}

MarkerCanvas.prototype.addMarkerStart = function () {
    this.addMarkerFlag = true;
}

MarkerCanvas.prototype.moveMarkerStart = function () {
    this.moveMarkerFlag = true;
}

MarkerCanvas.prototype.addMarker = function (markerData) {
    // init marker image
    const image = new createjs.Bitmap(this.markerImage);
    image.scale = this.markerImageScale;
    imageWidth = this.markerImage.width * this.markerImageScale

    // init marker text
    // see https://createjs.com/docs/easeljs/classes/Text.html for detail
    const text = new createjs.Text(markerData.text, "11px Arial", "blue")
    text.textAlign = "center";
    text.x = imageWidth / 2;
    text.y = -12;

    // add into container
    const newMarker = new createjs.Container();
    newMarker.addChild(image);
    newMarker.addChild(text);
    newMarker.data = markerData;

    // add event listeners
    // bubbles when touched
    bubbleScale = 1.2
    locOffset = (bubbleScale - 1) * imageWidth / 2;
    newMarker.addEventListener("mousedown", event => {
        event.stopPropagation();
        newMarker.scale *= bubbleScale;
        newMarker.x -= locOffset;
        this.hasUpdate = true;
    });
    newMarker.addEventListener("pressmove", event => {
        event.stopPropagation();
        if (this.moveMarkerFlag) {
            // only temporal display
            // does not change actual data
            newMarker.x = event.stageX;
            newMarker.y = event.stageY;
            this.hasUpdate = true;
        }
    })
    newMarker.addEventListener("pressup", event => {
        event.stopPropagation();
        newMarker.scale = 1;
        newMarker.x += locOffset;
        this.selectedMarker = newMarker;

        // if the marker was moved
        if (this.moveMarkerFlag) {
            this.moveMarkerFlag = false;
            // change relative location data
            relLoc = this.calcRelativeLocationInMain(event.stageX, event.stageY);
            newMarker.data.x = relLoc.x;
            newMarker.data.y = relLoc.y;
            this.calcMarkerLocation();
        }
        this.hasUpdate = true;
    });

    // save in marker list
    this.markers.push(newMarker);

    // show time
    this.stage.addChild(newMarker);
    this.calcMarkerLocation();
    this.hasUpdate = true;
}

MarkerCanvas.prototype.modifyMarker = function (marker, markerData) {
    markerText = marker.getChildAt(1);
    markerText.text = markerData.text;
    marker.data = markerData;
    this.calcMarkerLocation();
    this.hasUpdate = true;
}

MarkerCanvas.prototype.removeMarker = function (marker) {
    this.stage.removeChild(marker);
    const targetIndex = this.markers.indexOf(marker);
    this.markers.splice(targetIndex, 1);
    this.hasUpdate = true;
}

MarkerCanvas.prototype.clearAllMarkers = function () {
    this.markers.forEach(marker => {
        this.stage.removeChild(marker);
    });
    this.markers = [];
    this.hasUpdate = true;
}

MarkerCanvas.prototype.calcRelativeLocationInMain = function (stageX, stageY) {
    return {
        x: (stageX + (-this.mainBmp.x)) / this.mainBmp.width,
        y: (stageY + (-this.mainBmp.y)) / this.mainBmp.height
    };
}

MarkerCanvas.prototype.calcMarkerLocation = function () {
    this.markers.forEach(marker => {
        marker.x = this.mainBmp.x + this.mainBmp.width * marker.data.x;
        marker.y = this.mainBmp.y + this.mainBmp.height * marker.data.y;
    });
}

MarkerCanvas.prototype.extractMarkerData = function () {
    return this.markers.map(marker => marker.data);
}